import statusCode from './status-code'

import {
	ResponseNotFoundException,
	ResponseUnprocessableEntityException,
	ResponseBadRequestException,
	ResponseUnauthorizedException
} from './exceptions'

const {
	OK, 
	CREATED_OK,
	NOT_CONTENT,
	NOT_FOUND,
	BAD_REQUEST,
	UNPROCESSABLE_ENTITY,
	UNAUTHORIZED,
	INTERNAL_ERROR
} = statusCode

class Response {
    statusCode: number;
    body: string;
	constructor(statusCode, body = { }) {
		this.statusCode = statusCode
		this.body = JSON.stringify(body)
	}
}

class ResponseOK {
    statusCode: number;
    body: string;
	constructor(body = {}) {
		this.statusCode = OK
		this.body = JSON.stringify(body)
	}
}

class ResponseCreated {
    statusCode: number;
    body: string;
	constructor(body = {}) {
		this.statusCode = CREATED_OK
		this.body = JSON.stringify(body)
	}
}

class ResponseNotContent {
    statusCode: number;
    body: string;
	constructor() {
		this.statusCode = NOT_CONTENT
		this.body = null
	}
}

class ResponseError {
    statusCode: number;
    body: string;
	constructor(e, origin = 'internal') {
		if ( e instanceof ResponseNotFoundException ) {
			this.statusCode = NOT_FOUND
			this.body = this.mountBody(e.message, NOT_FOUND, origin) 
		} else if (e instanceof ResponseUnprocessableEntityException ) {
			this.statusCode = UNPROCESSABLE_ENTITY
			this.body = this.mountBody(e.message, UNPROCESSABLE_ENTITY, origin) 
		} else if (e instanceof ResponseBadRequestException) {
			this.statusCode = BAD_REQUEST
			this.body = this.mountBody(e.message, BAD_REQUEST, origin) 
		} else if (e instanceof ResponseUnauthorizedException) {
			this.statusCode = UNAUTHORIZED
			this.body = this.mountBody(e.message, UNAUTHORIZED, origin) 
		} else {
			this.statusCode = INTERNAL_ERROR
			this.body = this.mountBody(e.message, INTERNAL_ERROR, origin) 
		}
	}

	mountBody (message, errorCode, origin) {
		return JSON.stringify({ message, errorCode, origin })
	}
}

export { Response }
export { ResponseOK }
export { ResponseCreated }
export { ResponseNotContent }
export { ResponseError }