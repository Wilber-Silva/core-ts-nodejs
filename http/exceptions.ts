class ResponseNotFoundException extends Error {
	constructor(message) {
		super(message)
		this.name = 'ResponseNotFoundException'
	}
}

class ResponseUnprocessableEntityException extends Error {
	constructor(message) {
		super(message)
		this.name = 'ResponseUnprocessableEntityException'
	}
}

class ResponseBadRequestException extends Error {
	constructor(message) {
		super(message)
		this.name = 'ResponseBadRequestException'
	}
}

class ResponseUnauthorizedException extends Error {
	constructor(message) {
		super(message)
		this.name = 'ResponseUnauthorizedException'
	}
}

export { ResponseNotFoundException }
export { ResponseUnprocessableEntityException }
export { ResponseBadRequestException }
export { ResponseUnauthorizedException }