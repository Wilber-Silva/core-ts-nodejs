export default async function validationJoiSchema (schema, body) {
	const { error } = schema.validate(body)
	if (error) {
		return { isValid: false, message: error.details[0].message }
	}
	return { isValid: true }
}